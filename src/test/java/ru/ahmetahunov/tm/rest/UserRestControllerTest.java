package ru.ahmetahunov.tm.rest;

import org.jetbrains.annotations.Nullable;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserRestControllerTest extends AbstractRestTestConfig {

	@Autowired
	private IUserService userService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void getUsersJSON() throws Exception {
		mockMvc.perform(get("/users")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void getUsersXML() throws Exception {
		mockMvc.perform(get("/users")
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void getUserJSON() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(get("/users/" + user.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.login").value(user.getLogin()))
				.andExpect(jsonPath("$.id").value(user.getId()));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void getUserXML() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(get("/users/" + user.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("UserDTO/login").string(user.getLogin()))
				.andExpect(xpath("UserDTO/id").string(user.getId()));
	}

	@Test
	public void createUserJSON() throws Exception {
		assertNull(userService.findByLogin("test"));
		mockMvc.perform(post("/users")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"login\":\"test\",\"password\":\"123\",\"passwordCheck\":\"123\"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.login").value("test"));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		userService.remove(test.getId());
		assertNull(userService.findByLogin("test"));
	}

	@Test
	public void createUserXML() throws Exception {
		assertNull(userService.findByLogin("test"));
		mockMvc.perform(post("/users")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<UserDTO>" +
							"<login>test</login>" +
							"<password>123</password>" +
							"<passwordCheck>123</passwordCheck>" +
						"</UserDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		userService.remove(test.getId());
		assertNull(userService.findByLogin("test"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void createUserAdminJSON() throws Exception {
		assertNull(userService.findByLogin("test"));
		mockMvc.perform(post("/users/admin")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"login\":\"test\",\"password\":\"123\",\"roles\": [\"USER\", \"ADMINISTRATOR\"]}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.login").value("test"));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		assertEquals(2, test.getRoles().size());
		userService.remove(test.getId());
		assertNull(userService.findByLogin("test"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void createUserAdminXML() throws Exception {
		assertNull(userService.findByLogin("test"));
		mockMvc.perform(post("/users/admin")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<UserDTO>" +
							"<login>test</login>" +
							"<password>123</password>" +
							"<roles>" +
								"<roles>USER</roles>" +
								"<roles>ADMINISTRATOR</roles>" +
							"</roles>" +
						"</UserDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		assertEquals(2, test.getRoles().size());
		userService.remove(test.getId());
		assertNull(userService.findByLogin("test"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void editUserJSON() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(put("/users")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"login\":\"test\"," +
						"\"password\":\"user\"," +
						" \"passwordCheck\":\"user\"," +
						" \"passwordNew\":\"123\"}"
				))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		assertNotEquals(test.getPassword(), user.getPassword());
		userService.merge(user);
		assertNotNull(userService.findByLogin("user"));
		assertNull(userService.findByLogin("test"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void editUserXML() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(put("/users")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<UserDTO>" +
						"<login>test</login>" +
						"<password>user</password>" +
						"<passwordCheck>user</passwordCheck>" +
						"<passwordNew>123</passwordNew>" +
						"</UserDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		@Nullable final User test = userService.findByLogin("test");
		assertNotNull(test);
		assertNotEquals(test.getPassword(), user.getPassword());
		userService.merge(user);
		assertNotNull(userService.findByLogin("user"));
		assertNull(userService.findByLogin("test"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void editUserAdminJSON() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		assertEquals(1, user.getRoles().size());
		mockMvc.perform(put("/users/admin")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"id\":\"" + user.getId() + "\"," +
						"\"passwordNew\":\"123\"," +
						" \"roles\":[\"USER\", \"ADMINISTRATOR\"]}"
				))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
		@Nullable final User test = userService.findByLogin("user");
		assertNotNull(test);
		assertNotEquals(test.getPassword(), user.getPassword());
		assertEquals(2, test.getRoles().size());
		userService.remove(test.getId());
		userService.persist(user);
		assertEquals(1, userService.findByLogin("user").getRoles().size());
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void editUserAdminXML() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		assertEquals(1, user1.getRoles().size());
		mockMvc.perform(put("/users/admin")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<UserDTO>" +
							"<id>" + user1.getId() + "</id>" +
							"<passwordNew>123</passwordNew>" +
							"<roles>" +
								"<roles>USER</roles>" +
								"<roles>ADMINISTRATOR</roles>" +
							"</roles>" +
						"</UserDTO>"
				))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		@Nullable final User test = userService.findByLogin("user");
		assertNotNull(test);
		assertNotEquals(test.getPassword(), user1.getPassword());
		assertEquals(2, test.getRoles().size());
		userService.remove(test.getId());
		userService.persist(user1);
		assertEquals(1, userService.findByLogin("user").getRoles().size());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void removeUser() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(delete("/users"))
				.andExpect(status().isOk());
		assertNull(userService.findByLogin("user"));
		userService.persist(user);
		assertNotNull(userService.findByLogin("user"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void removeUserAdmin() throws Exception {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		mockMvc.perform(delete("/users/" + user.getId()))
				.andExpect(status().isOk());
		assertNull(userService.findByLogin("user"));
		userService.persist(user);
		assertNotNull(userService.findByLogin("user"));
	}

}