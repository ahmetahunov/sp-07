package ru.ahmetahunov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.Assert.*;

public class ProjectRestControllerTest extends AbstractRestTestConfig {

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	@Autowired
	private MockMvc mockMvc;

	private Project project;

	private User user;

	@Before
	public void prepare() {
		user = userService.findByLogin("user");
		project = new Project();
		project.setName("test");
		project.setUser(user);
		projectService.persist(project);
	}

	@After
	public void clear() {
		for (@NotNull final Project project : projectService.findAll(user.getId()))
			projectService.remove(project.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getProjectJSON() throws Exception {
		mockMvc.perform(get("/projects/" + project.getId()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(project.getId()))
				.andExpect(jsonPath("$.name").value(project.getName()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getProjectXML() throws Exception {
		mockMvc.perform(get("/projects/" + project.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/ProjectDTO/id").string(project.getId()))
				.andExpect(xpath("/ProjectDTO/name").string(project.getName()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getProjectsJSON() throws Exception {
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		project1.setUser(user);
		projectService.persist(project1);
		assertEquals(2, projectService.findAll(user.getId()).size());
		mockMvc.perform(get("/projects")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.length()").value(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getProjectsXML() throws Exception {
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		project1.setUser(user);
		projectService.persist(project1);
		assertEquals(2, projectService.findAll(user.getId()).size());
		mockMvc.perform(get("/projects")
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/List/item").nodeCount(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void addProjectJSON() throws Exception {
		int count = projectService.findAll(user.getId()).size();
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setUserId(user.getId());
		projectDTO.setName("test");
		mockMvc.perform(post("/projects")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{" +
							"\"id\":\"" + projectDTO.getId() + "\"," +
							"\"name\":\"" + projectDTO.getName() + "\"," +
							"\"userId\":\"" + projectDTO.getUserId() + "\"" +
						"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
		assertEquals(1, projectService.findAll(user.getId()).size() - count);
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void addProjectXML() throws Exception {
		int count = projectService.findAll(user.getId()).size();
		@NotNull final ProjectDTO projectDTO = new ProjectDTO();
		projectDTO.setUserId(user.getId());
		projectDTO.setName("test");
		mockMvc.perform(post("/projects")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<ProjectDTO>" +
							"<id>" + projectDTO.getId() + "</id>" +
							"<name>" + projectDTO.getName() + "</name>" +
							"<userId>" + projectDTO.getUserId() + "</userId>" +
						"</ProjectDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		assertEquals(1, projectService.findAll(user.getId()).size() - count);
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void updateProjectJSON() throws Exception {
		mockMvc.perform(post("/projects")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{" +
						"\"id\":\"" + project.getId() + "\"," +
						"\"name\":\"testChanged\"," +
						"\"description\":\"testChanged\"," +
						"\"userId\":\"" + user.getId() + "\"" +
						"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		assertEquals("testChanged", found.getName());
		assertEquals("testChanged", found.getName());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void updateProjectXML() throws Exception {
		mockMvc.perform(post("/projects")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<ProjectDTO>" +
							"<id>" + project.getId() + "</id>" +
							"<name>testChanged</name>" +
							"<description>testChanged</description>" +
							"<userId>" + user.getId() + "</userId>" +
						"</ProjectDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE));
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		assertEquals("testChanged", found.getName());
		assertEquals("testChanged", found.getName());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void removeProject() throws Exception {
		assertNotNull(projectService.findOne(project.getId()));
		mockMvc.perform(delete("/projects/" + project.getId()))
				.andExpect(status().isOk());
		assertNull(projectService.findOne(project.getId()));
	}

}