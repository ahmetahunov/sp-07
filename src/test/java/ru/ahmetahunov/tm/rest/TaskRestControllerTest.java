package ru.ahmetahunov.tm.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskRestControllerTest extends AbstractRestTestConfig {

	@Autowired
	private ITaskService taskService;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	@Autowired
	private MockMvc mockMvc;

	private User user;

	private Project project;

	private Task task;

	@Before
	public void setup() {
		user = userService.findByLogin("user");
		project = new Project();
		project.setName("test");
		projectService.persist(project);
		task = new Task();
		task.setUser(user);
		task.setName("test");
		task.setProject(project);
		taskService.persist(task);
	}

	@After
	public void clear() {
		if (project == null) return;
		projectService.remove(project.getId());
		project = null;
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTaskJSON() throws Exception {
		mockMvc.perform(get("/tasks/" + task.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(task.getId()))
				.andExpect(jsonPath("$.name").value(task.getName()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTaskXML() throws Exception {
		mockMvc.perform(get("/tasks/" + task.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/TaskDTO/id").string(task.getId()))
				.andExpect(xpath("/TaskDTO/name").string(task.getName()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTasksJSON() throws Exception {
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setProject(project);
		task1.setUser(user);
		taskService.persist(task1);
		assertEquals(2, taskService.findAll(user.getId()).size());
		mockMvc.perform(get("/tasks")
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.length()").value(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTasksXML() throws Exception {
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setProject(project);
		task1.setUser(user);
		taskService.persist(task1);
		assertEquals(2, taskService.findAll(user.getId()).size());
		mockMvc.perform(get("/tasks")
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/List/item").nodeCount(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTasksByProjectJSON() throws Exception {
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setProject(project);
		task1.setUser(user);
		taskService.persist(task1);
		assertEquals(2, taskService.findAll(user.getId()).size());
		mockMvc.perform(get("/tasks/project/" + project.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.length()").value(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getTasksByProjectXML() throws Exception {
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setProject(project);
		task1.setUser(user);
		taskService.persist(task1);
		assertEquals(2, taskService.findAll(user.getId()).size());
		mockMvc.perform(get("/tasks/project/" + project.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/List/item").nodeCount(2));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void addTaskJSON() throws Exception {
		@NotNull final Task task1 = new Task();
		assertNull(taskService.findOne(task1.getId()));
		mockMvc.perform(post("/tasks")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON)
				.content("{" +
						"\"id\":\"" + task1.getId() + "\"," +
						"\"name\":\"" + task.getName() + "\"," +
						"\"userId\":\"" + user.getId() + "\"," +
						"\"projectId\":\"" + project.getId() + "\"" +
						"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(task1.getId()));
		assertNotNull(taskService.findOne(task1.getId()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void addTaskXML() throws Exception {
		@NotNull final Task task1 = new Task();
		assertNull(taskService.findOne(task1.getId()));
		mockMvc.perform(post("/tasks")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<TaskDTO>" +
							"<id>" + task1.getId() + "</id>" +
							"<name>" + task.getName() + "</name>" +
							"<userId>" + user.getId() + "</userId>" +
							"<projectId>" + project.getId() + "</projectId>" +
						"</TaskDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/TaskDTO/id").string(task1.getId()));
		assertNotNull(taskService.findOne(task1.getId()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void updateTaskJSON() throws Exception {
		assertNotNull(taskService.findOne(task.getId()));
		mockMvc.perform(post("/tasks")
				.accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON)
				.content("{" +
						"\"id\":\"" + task.getId() + "\"," +
						"\"name\":\"testChanged\"," +
						"\"description\":\"testChanged\"," +
						"\"userId\":\"" + user.getId() + "\"," +
						"\"projectId\":\"" + project.getId() + "\"" +
						"}"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(task.getId()));
		@Nullable final Task found = taskService.findOne(task.getId());
		assertNotNull(found);
		assertEquals("testChanged", found.getName());
		assertEquals("testChanged", found.getDescription());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void updateTaskXML() throws Exception {
		assertNotNull(taskService.findOne(task.getId()));
		mockMvc.perform(post("/tasks")
				.accept(MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML_VALUE)
				.content("<TaskDTO>" +
							"<id>" + task.getId() + "</id>" +
							"<name>testChanged</name>" +
							"<description>testChanged</description>" +
							"<userId>" + user.getId() + "</userId>" +
							"<projectId>" + project.getId() + "</projectId>" +
						"</TaskDTO>"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/TaskDTO/id").string(task.getId()));
		@Nullable final Task found = taskService.findOne(task.getId());
		assertNotNull(found);
		assertEquals("testChanged", found.getName());
		assertEquals("testChanged", found.getDescription());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void removeTask() throws Exception {
		assertNotNull(taskService.findOne(task.getId()));
		mockMvc.perform(delete("/tasks/" + task.getId()))
				.andExpect(status().isOk());
		assertNull(taskService.findOne(task.getId()));
	}

}