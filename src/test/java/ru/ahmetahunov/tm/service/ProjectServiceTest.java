package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

public class ProjectServiceTest extends AbstractTestService {

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	private Project project;

	@Before
	public void prepare() {
		project = new Project();
		project.setName("test");
		projectService.persist(project);
	}

	@After
	public void clear() {
		if (project == null) return;
		projectService.remove(project.getId());
		project = null;
	}

	@Test
	public void persist() {
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		assertNull(projectService.persist(null));
		found.setName("");
		found.setId("123");
		assertNull(projectService.persist(found));
	}

	@Test
	public void merge() {
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		found.setName("changedName");
		found.setDescription("changedDescription");
		projectService.merge(found);
		@Nullable final Project changed = projectService.findOne(project.getId());
		assertNotNull(changed);
		assertEquals("changedName", changed.getName());
		assertEquals("changedDescription", changed.getDescription());
		found.setName("");
		assertNull(projectService.merge(found));
	}

	@Test
	public void findOneById() {
		assertNotNull(projectService.findOne(project.getId()));
		assertNull(projectService.findOne("unknown"));
		assertNull(projectService.findOne(null));
	}

	@Test
	public void findOneByUserIdAndId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		assertNull(projectService.findOne(user.getId(), project.getId()));
		assertNull(projectService.findOne(admin.getId(), project.getId()));
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		assertEquals(project.getId(), found.getId());
		found.setUser(user);
		projectService.merge(found);
		assertNotNull(projectService.findOne(user.getId(), project.getId()));
		assertNull(projectService.findOne(admin.getId(), project.getId()));
		assertNull(projectService.findOne(null, project.getId()));
		assertNull(projectService.findOne(admin.getId(), null));
	}

	@Test
	public void findAll() {
		int before = projectService.findAll().size();
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		projectService.persist(project1);
		assertEquals(1, projectService.findAll().size() - before);
		projectService.remove(project1.getId());
		assertEquals(0, projectService.findAll().size() - before);
	}

	@Test
	public void findAllByUserId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		int beforeUser = projectService.findAll(user.getId()).size();
		int beforeAdmin = projectService.findAll(admin.getId()).size();
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		project1.setUser(user);
		projectService.persist(project1);
		assertEquals(1, projectService.findAll(user.getId()).size() - beforeUser);
		assertEquals(0, projectService.findAll(admin.getId()).size() - beforeAdmin);
		projectService.remove(project1.getId());
		assertEquals(0, projectService.findAll(user.getId()).size() - beforeUser);
	}

	@Test
	public void removeById() {
		assertNotNull(projectService.findOne(project.getId()));
		projectService.remove(project.getId());
		assertNull(projectService.findOne(project.getId()));
		project = null;
	}

	@Test
	public void removeByUserIdAndId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		@Nullable final Project found = projectService.findOne(project.getId());
		assertNotNull(found);
		found.setUser(user);
		assertNull(projectService.findOne(user.getId(), found.getId()));
		projectService.merge(found);
		projectService.remove(admin.getId(), found.getId());
		assertNotNull(projectService.findOne(user.getId(), found.getId()));
		projectService.remove(user.getId(), found.getId());
		assertNull(projectService.findOne(user.getId(), project.getId()));
		project = null;
	}

}