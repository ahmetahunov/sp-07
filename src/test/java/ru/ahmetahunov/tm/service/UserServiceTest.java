package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

@Transactional
public class UserServiceTest extends AbstractTestService {

	@Autowired
	private IUserService userService;

	private User user;

	@Before
	public void prepare() {
		user = new User();
		user.setLogin("test");
		user.setPassword("1234");
		userService.persist(user);
	}

	@After
	public void clear() {
		if (user == null) return;
		userService.remove(user.getId());
		user = null;
	}

	@Test
	public void persist() {
		@Nullable final User found = userService.findByLogin("test");
		assertNotNull(found);
		assertNull(userService.persist(null));
		found.setId("123");
		found.setLogin("");
		assertNull(userService.persist(found));
		found.setId("456");
		found.setLogin(null);
		assertNull(userService.persist(found));
	}

	@Test
	public void persistFailed() {
		@Nullable final User user1 = new User();
		user1.setLogin("test");
		user1.setPassword("123");
		assertNull(userService.persist(user1));
	}

	@Test(expected = Exception.class)
	public void persistNullPasswordFailed() {
		@Nullable final User user1 = new User();
		user1.setLogin("test1");
		userService.persist(user1);
	}

	@Test
	public void merge() {
		assertNotNull(userService.findByLogin("test"));
		user.setLogin("changedTest");
		user.setPassword("1111");
		userService.merge(user);
		@Nullable final User found = userService.findByLogin("changedTest");
		assertNotNull(found);
		assertEquals("1111", found.getPassword());
		assertNull(userService.findByLogin("test"));
		assertNull(userService.merge(null));
		found.setId("123");
		found.setLogin("");
		assertNull(userService.merge(found));
		found.setId("456");
		found.setLogin(null);
		assertNull(userService.merge(found));
	}

	@Test
	public void findOne() {
		assertNotNull(userService.findOne(user.getId()));
		assertNull(userService.findOne("unknown"));
		assertNull(userService.findOne(""));
		assertNull(userService.findOne(null));
	}

	@Test
	public void findAll() {
		int before = userService.findAll().size();
		@NotNull final User user1 = new User();
		user1.setLogin("test1");
		user1.setPassword("123");
		userService.persist(user1);
		assertEquals(1, userService.findAll().size() - before);
		userService.remove(user1.getId());
		assertEquals(0, userService.findAll().size() - before);
	}

	@Test
	public void findByLogin() {
		assertNotNull(userService.findByLogin("test"));
		assertNull(userService.findByLogin("unknown"));
		assertNull(userService.findByLogin(""));
		assertNull(userService.findByLogin(null));
	}

	@Test
	public void remove() {
		assertNotNull(userService.findOne(user.getId()));
		userService.remove(user.getId());
		assertNull(userService.findOne(user.getId()));
		user = null;
	}

}