package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

public class TaskServiceTest extends AbstractTestService {

	@Autowired
	private ITaskService taskService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IProjectService projectService;

	private Task task;

	@Before
	public void prepare() {
		task = new Task();
		task.setName("test");
		taskService.persist(task);
	}

	@After
	public void clear() {
		if (task == null) return;
		taskService.remove(task.getId());
		task = null;
	}

	@Test
	public void persist() {
		@Nullable final Task found = taskService.findOne(task.getId());
		assertNotNull(found);
		assertNull(taskService.persist(null));
		found.setId("123");
		found.setName("");
		assertNull(taskService.persist(found));
	}

	@Test
	public void merge() {
		@Nullable final Task found = taskService.findOne(task.getId());
		assertNotNull(found);
		found.setName("changedName");
		found.setDescription("changedDescription");
		taskService.merge(found);
		@Nullable final Task changed = taskService.findOne(task.getId());
		assertNotNull(changed);
		assertEquals("changedName", changed.getName());
		assertEquals("changedDescription", changed.getDescription());
		found.setName("");
		assertNull(taskService.merge(found));
	}

	@Test
	public void findOneById() {
		assertNotNull(taskService.findOne(task.getId()));
		assertNull(taskService.findOne("unknown"));
		assertNull(taskService.findOne(null));
	}

	@Test
	public void findOneByUserIdAndId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		task.setUser(user);
		taskService.merge(task);
		assertNotNull(taskService.findOne(user.getId(), task.getId()));
		assertNull(taskService.findOne(admin.getId(), task.getId()));
		assertNull(taskService.findOne(null, task.getId()));
		assertNull(taskService.findOne(admin.getId(), null));
	}

	@Test
	public void findAll() {
		int before = taskService.findAll().size();
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		taskService.persist(task1);
		assertEquals(1, taskService.findAll().size() - before);
		taskService.remove(task1.getId());
		assertEquals(0, taskService.findAll().size() - before);
	}

	@Test
	public void findAllByUserId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		int beforeUser = taskService.findAll(user.getId()).size();
		int beforeAdmin = taskService.findAll(admin.getId()).size();
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setUser(user);
		taskService.merge(task1);
		assertEquals(1, taskService.findAll(user.getId()).size() - beforeUser);
		assertEquals(0, taskService.findAll(admin.getId()).size() - beforeAdmin);
		taskService.remove(task1.getId());
		assertEquals(0, taskService.findAll(user.getId()).size() - beforeUser);
	}

	@Test
	public void findAllByUserIdAndProjectId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		@NotNull final Project project = new Project();
		project.setName("test");
		project.setUser(user);
		projectService.persist(project);
		int beforeUser = taskService.findAll(user.getId(), project.getId()).size();
		int beforeAdmin = taskService.findAll(admin.getId(), project.getId()).size();
		@NotNull final Task task1 = new Task();
		task1.setName("test1");
		task1.setUser(user);
		task1.setProject(project);
		taskService.merge(task1);
		assertEquals(1, taskService.findAll(user.getId(), project.getId()).size() - beforeUser);
		assertEquals(0, taskService.findAll(admin.getId(), project.getId()).size() - beforeAdmin);
		taskService.remove(task1.getId());
		assertEquals(0, taskService.findAll(user.getId(), project.getId()).size() - beforeUser);
		projectService.remove(project.getId());
		assertNull(projectService.findOne(project.getId()));
	}

	@Test
	public void removeById() {
		assertNotNull(taskService.findOne(task.getId()));
		taskService.remove(task.getId());
		assertNull(taskService.findOne(task.getId()));
		task = null;
	}

	@Test
	public void removeByUserIdAndId() {
		@Nullable final User user = userService.findByLogin("user");
		@Nullable final User admin = userService.findByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		task.setUser(user);
		taskService.merge(task);
		assertNotNull(taskService.findOne(user.getId(), task.getId()));
		assertNull(taskService.findOne(admin.getId(), task.getId()));
		taskService.remove(admin.getId(), task.getId());
		assertNotNull(taskService.findOne(user.getId(), task.getId()));
		taskService.remove(user.getId(), task.getId());
		assertNull(taskService.findOne(user.getId(), task.getId()));
		task = null;
	}

}