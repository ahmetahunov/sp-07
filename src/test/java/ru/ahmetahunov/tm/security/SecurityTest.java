package ru.ahmetahunov.tm.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class SecurityTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void wrongPasswordTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("user").password("unknown"))
				.andExpect(unauthenticated());
	}

	@Test
	public void correctPasswordTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("admin").password("admin"))
				.andExpect(authenticated());
	}

	@Test
	public void wrongUserTest() throws Exception {
		mockMvc.perform(formLogin("/login").user("unknown").password("user"))
				.andExpect(unauthenticated());
	}

	@Test
	public void withoutUserDataTest() throws Exception {
		mockMvc.perform(get("/projects"))
				.andExpect(status().is4xxClientError());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userTest() throws Exception {
		mockMvc.perform(get("/projects"))
				.andExpect(status().isOk())
				.andExpect(authenticated().withRoles("USER"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userFailedWithoutAdminRoleTest() throws Exception {
		mockMvc.perform(get("/users"))
				.andExpect(status().is4xxClientError());
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void adminRoleTest() throws Exception {
		mockMvc.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(authenticated().withRoles("USER", "ADMINISTRATOR"));
	}

}
