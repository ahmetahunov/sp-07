package ru.ahmetahunov.tm.marshalling;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Role;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.RoleType;
import ru.ahmetahunov.tm.enumerated.Status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.Assert.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class MarshallingTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private IUserService userService;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private ITaskService taskService;

	private User user;

	private Project project;

	private Task task;

	@After
	public void clear() {
		if (user != null) userService.remove(user.getId());
		if (project != null) projectService.remove(project.getId());
		user = null;
		project = null;
		task = null;
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void userTestJson() throws Exception {
		user = new User();
		user.setLogin("test");
		user.setPassword("123");
		@NotNull final Role role = new Role();
		role.setUser(user);
		role.setType(RoleType.USER);
		user.getRoles().add(role);
		userService.persist(user);
		assertNotNull(userService.findByLogin(user.getLogin()));
		mockMvc.perform(get("/users/" + user.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(user.getId()))
				.andExpect(jsonPath("$.login").value(user.getLogin()))
				.andExpect(jsonPath("$.password").value(user.getPassword()))
				.andExpect(jsonPath("$.roles[0]").value(RoleType.USER.toString()));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void userTestXML() throws Exception {
		user = new User();
		user.setLogin("test");
		user.setPassword("123");
		@NotNull final Role role = new Role();
		role.setUser(user);
		role.setType(RoleType.USER);
		user.getRoles().add(role);
		userService.persist(user);
		assertNotNull(userService.findByLogin(user.getLogin()));
		mockMvc.perform(get("/users/" + user.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/UserDTO/id").string(user.getId()))
				.andExpect(xpath("/UserDTO/login").string(user.getLogin()))
				.andExpect(xpath("/UserDTO/password").string(user.getPassword()))
				.andExpect(xpath("/UserDTO/roles[1]").string(RoleType.USER.toString()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectTestJSON() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		project = new Project();
		project.setUser(user1);
		project.setName("test");
		project.setDescription("description");
		project.setStatus(Status.IN_PROGRESS);
		projectService.persist(project);
		mockMvc.perform(get("/projects/" + project.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(project.getId()))
				.andExpect(jsonPath("$.name").value(project.getName()))
				.andExpect(jsonPath("$.description").value(project.getDescription()))
				.andExpect(jsonPath("$.status").value(project.getStatus().toString()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectTestXML() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		project = new Project();
		project.setUser(user1);
		project.setName("test");
		project.setDescription("description");
		project.setStatus(Status.IN_PROGRESS);
		projectService.persist(project);
		mockMvc.perform(get("/projects/" + project.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/ProjectDTO/id").string(project.getId()))
				.andExpect(xpath("/ProjectDTO/name").string(project.getName()))
				.andExpect(xpath("/ProjectDTO/description").string(project.getDescription()))
				.andExpect(xpath("/ProjectDTO/status").string(project.getStatus().toString()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskTestJSON() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		project = new Project();
		project.setUser(user1);
		project.setName("test");
		projectService.persist(project);
		task = new Task();
		task.setUser(user1);
		task.setProject(project);
		task.setName("test");
		task.setDescription("description");
		taskService.persist(task);
		mockMvc.perform(get("/tasks/" + task.getId())
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").value(task.getId()))
				.andExpect(jsonPath("$.name").value(task.getName()))
				.andExpect(jsonPath("$.description").value(task.getDescription()))
				.andExpect(jsonPath("$.status").value(task.getStatus().toString()));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskTestXML() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		project = new Project();
		project.setUser(user1);
		project.setName("test");
		projectService.persist(project);
		task = new Task();
		task.setUser(user1);
		task.setProject(project);
		task.setName("test");
		task.setDescription("description");
		taskService.persist(task);
		mockMvc.perform(get("/tasks/" + task.getId())
				.accept(MediaType.APPLICATION_XML_VALUE))
				.andExpect(xpath("/TaskDTO/id").string(task.getId()))
				.andExpect(xpath("/TaskDTO/name").string(task.getName()))
				.andExpect(xpath("/TaskDTO/description").string(task.getDescription()))
				.andExpect(xpath("/TaskDTO/status").string(task.getStatus().toString()));
	}

}
