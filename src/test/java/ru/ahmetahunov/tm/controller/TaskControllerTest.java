package ru.ahmetahunov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TaskControllerTest extends AbstractTestController {

	@Autowired
	private ITaskService taskService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private MockMvc mockMvc;

	private Task task;

	private Project project;

	private String userId;

	@Before
	public void prepare() {
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		userId = user.getId();
		project = new Project();
		project.setName("test");
		project.setUser(user);
		projectService.persist(project);
		task = new Task();
		task.setName("test");
		task.setUser(user);
		task.setProject(project);
		taskService.persist(task);
	}

	@After
	public void clear() {
		if (project == null) return;
		projectService.remove(project.getId());
		project = null;
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskListGet() throws Exception {
		mockMvc
				.perform(get("/task_list"))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("tasks"))
				.andExpect(view().name("task/task_list"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskListByProjectIdGet() throws Exception {
		@NotNull final Project project = new Project();
		project.setName("test");
		task.setProject(project);
		projectService.persist(project);
		taskService.merge(task);
		mockMvc
				.perform(get("/task_list/" + project.getId()))
				.andExpect(status().isOk())
				.andExpect(model().attributeExists("tasks"))
				.andExpect(view().name("task/task_list_by_project"))
				.andReturn();
		projectService.remove(project.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskInfoGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/task_info/" + task.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("task/task_info"))
				.andExpect(model().attributeExists("task"))
				.andReturn();
		@Nullable final Task found = (Task) result
				.getModelAndView().getModel().get("task");
		assertNotNull(found);
		assertEquals(task.getName(), found.getName());
		assertEquals(task.getDescription(), found.getDescription());
		assertEquals(task.getStatus(), found.getStatus());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskCreateGet() throws Exception {
		mockMvc
				.perform(get("/task_create"))
				.andExpect(status().isOk())
				.andExpect(view().name("task/task_create"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskCreatePost() throws Exception {
		@NotNull final Project project = new Project();
		project.setName("test");
		projectService.persist(project);
		@NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		@NotNull final String dateString = simpleDateFormat.format(new Date());
		@NotNull final Date date = simpleDateFormat.parse(dateString);
		assertEquals(0, taskService.findAll(userId, project.getId()).size());
		mockMvc
				.perform(
						post("/task_create")
								.param("projectId", project.getId())
								.param("name", "test1")
								.param("startDate", dateString)
								.param("finishDate", dateString)
								.param("status", "IN_PROGRESS")
				)
				.andExpect(status().is3xxRedirection());
		@NotNull final List<Task> tasks = taskService.findAll(userId, project.getId());
		assertEquals(1, tasks.size());
		@NotNull final Task found = tasks.get(0);
		assertEquals(project.getId(), found.getProject().getId());
		assertEquals("test1", found.getName());
		assertEquals(date, found.getStartDate());
		assertEquals(date, found.getFinishDate());
		assertEquals("IN_PROGRESS", found.getStatus().toString());
		projectService.remove(project.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskUpdateGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/task_update/" + task.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("task/task_update"))
				.andExpect(model().attributeExists("taskEdit"))
				.andReturn();
		@Nullable final Task found = (Task) result.getModelAndView().getModel().get("taskEdit");
		assertNotNull(found);
		assertEquals(task.getName(), found.getName());
		assertEquals(task.getDescription(), found.getDescription());
		assertEquals(task.getStatus(), found.getStatus());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskUpdatePost() throws Exception {
		@NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		@NotNull final String dateString = simpleDateFormat.format(new Date(1000L));
		@NotNull final Date date = simpleDateFormat.parse(dateString);
		@NotNull final Project project = new Project();
		project.setName("test");
		projectService.persist(project);
		mockMvc
				.perform(
						post("/task_update")
								.param("projectId", project.getId())
								.param("id", task.getId())
								.param("name", "changedTest")
								.param("description", "changedDescription")
								.param("startDate", dateString)
								.param("finishDate", dateString)
								.param("status", "DONE")
				)
				.andExpect(view().name("task/task_info"))
				.andExpect(status().isOk());
		@Nullable final Task found = taskService.findOne(task.getId());
		assertNotNull(found);
		assertEquals(task.getId(), found.getId());
		assertEquals("changedTest", found.getName());
		assertEquals("changedDescription", found.getDescription());
		assertEquals(date, found.getStartDate());
		assertEquals(date, found.getFinishDate());
		assertEquals("DONE", found.getStatus().toString());
		projectService.remove(project.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void taskDeleteGet() throws Exception {
		assertNotNull(taskService.findOne(task.getId()));
		mockMvc
				.perform(get("/task_delete/" + task.getId()))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/task_list"));
		assertNull(taskService.findOne(task.getId()));
	}

}