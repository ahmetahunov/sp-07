package ru.ahmetahunov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class UserControllerTest extends AbstractTestController {

	@Autowired
	private IUserService userService;

	@Autowired
	private MockMvc mockMvc;

	private User user;

	@Before
	public void prepare() {
		user = new User();
		user.setLogin("test");
		user.setPassword("123");
		user = userService.persist(user);
	}

	@After
	public void clear() {
		if (user == null) return;
		userService.remove(user.getId());
		user = null;
	}

	@Test
	public void registrationGet() throws Exception {
		mockMvc
				.perform(get("/registration"))
				.andExpect(status().isOk())
				.andExpect(view().name("user/registration"));
	}

	@Test
	public void registrationPost() throws Exception {
		int before = userService.findAll().size();
		mockMvc
				.perform(
						post("/registration")
								.param("login", "test1")
								.param("password", "123")
								.param("passwordCheck", "123")
				)
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/login"));
		assertEquals(1, userService.findAll().size() - before);
		userService.remove(userService.findByLogin("test1").getId());
		assertEquals(0, userService.findAll().size() - before);
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void userListGet() throws Exception {
		mockMvc
				.perform(get("/admin/user_list"))
				.andExpect(status().isOk())
				.andExpect(view().name("user/admin_user_list"))
				.andExpect(model().attributeExists("users"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void userEditAdminGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/admin/user_edit/" + user.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("user/admin_user_edit"))
				.andReturn();
		@Nullable final User found = (User) result.getModelAndView().getModel().get("user");
		assertNotNull(found);
		assertEquals(user.getLogin(), found.getLogin());
		assertEquals(user.getId(), found.getId());
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void userEditAdminPost() throws Exception {
		mockMvc
				.perform(
						post("/admin/user_edit")
								.param("id", user.getId())
								.param("login", user.getLogin())
								.param("passwordNew", "1")
								.param("roles", "USER", "ADMINISTRATOR")
				)
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/admin/user_list"));
		@Nullable final User found = userService.findOne(user.getId());
		assertNotNull(found);
		assertEquals(user.getLogin(), found.getLogin());
		assertNotEquals(user.getPassword(), found.getPassword());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userEditGet() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		@NotNull final MvcResult result = mockMvc
				.perform(get("/user/user_edit/" + user1.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("user/user_edit"))
				.andReturn();
		@Nullable final User updated = (User) result.getModelAndView().getModel().get("user");
		assertNotNull(updated);
		assertEquals(user1.getId(), updated.getId());
		assertEquals(user1.getLogin(), updated.getLogin());
		assertEquals(user1.getPassword(), updated.getPassword());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void userEditPost() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		mockMvc
				.perform(
						post("/user/user_edit")
								.param("id", user1.getId())
								.param("login", "changedLogin")
								.param("passwordNew", "1")
								.param("password", "user")
								.param("passwordCheck", "user")
				)
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/"));
		@Nullable final User edited = userService.findOne(user1.getId());
		assertNotNull(edited);
		assertNotEquals(user1.getLogin(), edited.getLogin());
		assertNotEquals(user1.getPassword(), edited.getPassword());
		userService.merge(user1);
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void registrationAdminGet() throws Exception {
		mockMvc
				.perform(get("/admin/registration"))
				.andExpect(status().isOk())
				.andExpect(view().name("user/admin_registration"));
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void registrationAdminPost() throws Exception {
		int before = userService.findAll().size();
		mockMvc
				.perform(
						post("/admin/registration")
								.param("login", "test1")
								.param("password", "123")
								.param("passwordCheck", "123")
								.param("roles", "USER", "ADMINISTRATOR")
				)
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/admin/user_list"));
		assertEquals(1, userService.findAll().size() - before);
		assertEquals(2, userService.findByLogin("test1").getRoles().size());
		userService.remove(userService.findByLogin("test1").getId());
		assertEquals(0, userService.findAll().size() - before);
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void removeUserGet() throws Exception {
		@Nullable final User user1 = userService.findByLogin("user");
		assertNotNull(user1);
		mockMvc.perform(get("/user/delete/" + user1.getId()))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/logout"));
		assertNull(userService.findOne(user1.getId()));
		userService.persist(user1);
	}

	@Test
	@WithUserDetails(value = "admin", userDetailsServiceBeanName = "userDetailsService")
	public void removeUserAdminGet() throws Exception {
		assertNotNull(userService.findOne(user.getId()));
		mockMvc.perform(get("/admin/delete/" + user.getId()))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/admin/user_list"));
		assertNull(userService.findOne(user.getId()));
		user = null;
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void getInfoGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/user/user_info"))
				.andExpect(status().isOk())
				.andExpect(view().name("user/user_info"))
				.andExpect(model().attributeExists("user"))
				.andReturn();
		@Nullable final User found = (User) result.getModelAndView().getModel().get("user");
		assertNotNull(found);
		assertEquals("user", found.getLogin());
	}

}