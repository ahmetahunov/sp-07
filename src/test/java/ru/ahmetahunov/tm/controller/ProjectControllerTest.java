package ru.ahmetahunov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectControllerTest extends AbstractTestController {

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	@Autowired
	private MockMvc mockMvc;

	private Project project;

	private String userId;

	@Before
	public void prepare() {
		project = new Project();
		@Nullable final User user = userService.findByLogin("user");
		assertNotNull(user);
		userId = user.getId();
		project.setName("test");
		project.setUser(user);
		projectService.persist(project);
	}

	@After
	public void clear() {
		for (@NotNull final Project project : projectService.findAll(userId))
			projectService.remove(project.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectListGet() throws Exception {
		mockMvc
				.perform(get("/project_list"))
				.andExpect(status().isOk())
				.andExpect(view().name("project/project_list"))
				.andExpect(model().attributeExists("projects"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectInfoGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/project_info/" + project.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("project/project_info"))
				.andExpect(model().attributeExists("project"))
				.andReturn();
		@Nullable final Project found = (Project) result
				.getModelAndView().getModel().get("project");
		assertNotNull(found);
		assertEquals(project.getName(), found.getName());
		assertEquals(project.getDescription(), found.getDescription());
		assertEquals(project.getStatus(), found.getStatus());
		assertEquals(project.getId(), found.getId());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectCreateGet() throws Exception {
		mockMvc
				.perform(get("/project_create"))
				.andExpect(status().isOk())
				.andExpect(view().name("project/project_create"));
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectCreatePost() throws Exception {
		projectService.remove(project.getId());
		project = null;
		assertEquals(0, projectService.findAll(userId).size());
		@NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		@NotNull final String dateString = simpleDateFormat.format(new Date());
		@NotNull final Date date = simpleDateFormat.parse(dateString);
		mockMvc
				.perform(
						post("/project_create")
						.param("name", "test")
						.param("description", "testDescription")
						.param("startDate", dateString)
						.param("finishDate", dateString)
						.param("status", "DONE")
				)
				.andExpect(redirectedUrl("/project_list"))
				.andExpect(status().is3xxRedirection());
		@NotNull final List<Project> projects = projectService.findAll(userId);
		assertEquals(1, projects.size());
		@NotNull final Project found = projects.get(0);
		assertEquals("test", found.getName());
		assertEquals("testDescription", found.getDescription());
		assertEquals(date, found.getStartDate());
		assertEquals(date, found.getFinishDate());
		assertEquals("DONE", found.getStatus().toString());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectUpdateGet() throws Exception {
		@NotNull final MvcResult result = mockMvc
				.perform(get("/project_update/" + project.getId()))
				.andExpect(status().isOk())
				.andExpect(view().name("project/project_update"))
				.andExpect(model().attributeExists("projectEdit"))
				.andReturn();
		@Nullable final Project updated = (Project) result
				.getModelAndView().getModel().get("projectEdit");
		assertNotNull(updated);
		assertEquals(project.getId(), updated.getId());
		assertEquals(project.getName(), updated.getName());
		assertEquals(project.getDescription(), updated.getDescription());
		assertEquals(project.getStatus(), updated.getStatus());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectUpdatePost() throws Exception {
		@NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		@NotNull final String dateString = simpleDateFormat.format(new Date(1000L));
		@NotNull final Date date = simpleDateFormat.parse(dateString);
		mockMvc
				.perform(
						post("/project_update")
								.param("id", project.getId())
								.param("name", "changedTest")
								.param("description", "changedDescription")
								.param("startDate", dateString)
								.param("finishDate", dateString)
								.param("status", "IN_PROGRESS")
				)
				.andExpect(view().name("project/project_info"))
				.andExpect(status().isOk());
		@Nullable final Project edited = projectService.findOne(project.getId());
		assertNotNull(edited);
		assertEquals(project.getId(), edited.getId());
		assertEquals("changedTest", edited.getName());
		assertEquals("changedDescription", edited.getDescription());
		assertEquals(date, edited.getStartDate());
		assertEquals(date, edited.getFinishDate());
		assertEquals("IN_PROGRESS", edited.getStatus().toString());
	}

	@Test
	@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
	public void projectDeleteGet() throws Exception {
		assertNotNull(projectService.findOne(project.getId()));
		mockMvc
				.perform(get("/project_delete/" + project.getId()))
				.andExpect(redirectedUrl("/project_list"))
				.andExpect(status().is3xxRedirection());
		assertNull(projectService.findOne(project.getId()));
	}

}