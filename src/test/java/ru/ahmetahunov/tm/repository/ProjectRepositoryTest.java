package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

@Transactional
public class ProjectRepositoryTest extends AbstractTestRepository {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private UserRepository userRepository;

	private Project project;

	@Before
	public void prepare() {
		this.project = new Project();
		project.setName("test");
		projectRepository.save(project);
	}

	@After
	public void clear() {
		if (project == null) return;
		projectRepository.delete(project);
		project = null;
	}

	@Test
	public void saveProjectTest() {
		@Nullable final Project found = projectRepository.findById(project.getId()).orElse(null);
		assertNotNull(found);
		assertEquals("test", found.getName());
	}

	@Test
	public void updateProjectTest() {
		project.setName("changedTest");
		project.setDescription("changeDescription");
		projectRepository.save(project);
		@Nullable final Project found = projectRepository.findById(project.getId()).orElse(null);
		assertNotNull(found);
		assertEquals("changedTest", found.getName());
		assertEquals("changeDescription", found.getDescription());
	}

	@Test
	public void findByIdProjectTest() {
		assertNotNull(projectRepository.findById(project.getId()).orElse(null));
		assertNull(projectRepository.findById("unknown").orElse(null));
		assertNull(projectRepository.findById("").orElse(null));
	}

	@Test
	public void findByUserIdAndIdProjectTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		project.setUser(user);
		projectRepository.save(project);
		assertNotNull(projectRepository.findByUserIdAndId(user.getId(), project.getId()));
		assertNull(projectRepository.findByUserIdAndId(admin.getId(), project.getId()));
	}

	@Test
	public void findAllProjectsTest() {
		int before = projectRepository.findAll().size();
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		projectRepository.save(project1);
		assertEquals(1, projectRepository.findAll().size() - before);
		projectRepository.delete(project1);
		assertEquals(0, projectRepository.findAll().size() - before);
	}

	@Test
	public void findAllByUserIdProjectsTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		int beforeUser = projectRepository.findAllByUserId(user.getId()).size();
		int beforeAdmin = projectRepository.findAllByUserId(admin.getId()).size();
		@NotNull final Project project1 = new Project();
		project1.setName("test1");
		project1.setUser(user);
		projectRepository.save(project1);
		assertEquals(1, projectRepository.findAllByUserId(user.getId()).size() - beforeUser);
		assertEquals(0, projectRepository.findAllByUserId(admin.getId()).size() - beforeAdmin);
		projectRepository.delete(project1);
		assertEquals(0, projectRepository.findAllByUserId(user.getId()).size() - beforeUser);
	}

	@Test
	public void deleteByIdTest() {
		assertNotNull(projectRepository.findById(project.getId()).orElse(null));
		projectRepository.deleteById(project.getId());
		assertNull(projectRepository.findById(project.getId()).orElse(null));
		project = null;
	}

	@Test
	public void deleteByUserIdAdnIdTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		project.setUser(user);
		projectRepository.save(project);
		assertNotNull(projectRepository.findByUserIdAndId(user.getId(), project.getId()));
		projectRepository.deleteByUserIdAndId(admin.getId(), project.getId());
		assertNotNull(projectRepository.findByUserIdAndId(user.getId(), project.getId()));
		projectRepository.deleteByUserIdAndId(user.getId(), project.getId());
		assertNull(projectRepository.findByUserIdAndId(user.getId(), project.getId()));
		project = null;
	}

}