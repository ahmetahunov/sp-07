package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

@Transactional
public class UserRepositoryTest extends AbstractTestRepository {

	@Autowired
	private UserRepository userRepository;

	private User user;

	@Before
	public void prepare() {
		this.user = new User();
		this.user.setLogin("test");
		this.user.setPassword("123");
		userRepository.save(user);
	}

	@After
	public void clear() {
		if (this.user == null) return;
		userRepository.delete(this.user);
		this.user = null;
	}

	@Test
	public void saveUserTest() {
		@Nullable final User created = userRepository.findById(user.getId()).orElse(null);
		assertNotNull(created);
		assertEquals("test", created.getLogin());
	}

	@Test
	public void updateUserTest() {
		@Nullable final User created = userRepository.findById(user.getId()).orElse(null);
		assertNotNull(created);
		assertEquals("test", created.getLogin());
		user.setLogin("changedTest");
		userRepository.save(user);
		@Nullable final User changed = userRepository.findById(user.getId()).orElse(null);
		assertNotNull(changed);
		assertEquals("changedTest", changed.getLogin());
	}

	@Test
	public void findByIdTest() {
		assertNotNull(userRepository.findById(user.getId()).orElse(null));
		assertNull(userRepository.findById("unknown").orElse(null));
		assertNull(userRepository.findById("").orElse(null));
	}

	@Test
	public void findUserByLoginTest() {
		assertNotNull(userRepository.findUserByLogin("test"));
		assertNull(userRepository.findUserByLogin("unknown"));
		assertNull(userRepository.findUserByLogin(""));
	}

	@Test
	public void findAllTest() {
		int before = userRepository.findAll().size();
		@NotNull final User user1 = new User();
		user1.setLogin("test1");
		user1.setPassword("123");
		userRepository.save(user1);
		assertEquals(1, userRepository.findAll().size() - before);
		userRepository.delete(user1);
	}

	@Test
	public void removeUserTest() {
		userRepository.deleteById(user.getId());
		assertNull(userRepository.findById(user.getId()).orElse(null));
		user = null;
	}

}