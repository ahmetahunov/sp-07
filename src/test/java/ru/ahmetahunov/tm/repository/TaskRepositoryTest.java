package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.entity.User;
import static org.junit.Assert.*;

@Transactional
public class TaskRepositoryTest extends AbstractTestRepository {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private UserRepository userRepository;

	private Task task;

	@Before
	public void prepare() {
		this.task = new Task();
		task.setName("test");
		taskRepository.save(task);
	}

	@After
	public void clear() {
		if (task == null) return;
		taskRepository.delete(task);
		task = null;
	}

	@Test
	public void saveTaskTest() {
		@Nullable final Task created = taskRepository.findById(task.getId()).orElse(null);
		assertNotNull(created);
		assertEquals("test", created.getName());
	}

	@Test
	public void updateTaskTest() {
		task.setName("changedTest");
		task.setDescription("testDescription");
		taskRepository.save(task);
		@Nullable final Task changed = taskRepository.findById(task.getId()).orElse(null);
		assertNotNull(changed);
		assertEquals("changedTest", changed.getName());
		assertEquals("testDescription", changed.getDescription());
	}

	@Test
	public void findByIdTaskTest() {
		assertNotNull(taskRepository.findById(task.getId()).orElse(null));
		assertNull(taskRepository.findById("unknown").orElse(null));
		assertNull(taskRepository.findById("").orElse(null));
	}

	@Test
	public void findByUserIdAndIdTaskTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		task.setUser(user);
		taskRepository.save(task);
		assertNotNull(taskRepository.findByUserIdAndId(user.getId(), task.getId()));
		assertNull(taskRepository.findByUserIdAndId(admin.getId(), task.getId()));
	}

	@Test
	public void findAllTasksTest() {
		int before = taskRepository.findAll().size();
		@NotNull final Task task1 = new Task();
		task.setName("task1");
		taskRepository.save(task1);
		assertEquals(1, taskRepository.findAll().size() - before);
		taskRepository.delete(task1);
		assertEquals(0, taskRepository.findAll().size() - before);
	}

	@Test
	public void findAllByUserIdTaskTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		int beforeUser = taskRepository.findAllByUserId(user.getId()).size();
		int beforeAdmin = taskRepository.findAllByUserId(admin.getId()).size();
		@NotNull final Task task1 = new Task();
		task1.setName("task1");
		task1.setUser(user);
		taskRepository.save(task1);
		assertEquals(1, taskRepository.findAllByUserId(user.getId()).size() - beforeUser);
		assertEquals(0, taskRepository.findAllByUserId(admin.getId()).size() - beforeAdmin);
		taskRepository.delete(task1);
		assertEquals(0, taskRepository.findAllByUserId(user.getId()).size() - beforeUser);
	}

	@Test
	public void deleteByIdTest() {
		assertNotNull(taskRepository.findById(task.getId()).orElse(null));
		taskRepository.deleteById(task.getId());
		assertNull(taskRepository.findById(task.getId()).orElse(null));
		task = null;
	}

	@Test
	public void deleteByUserIdAdnIdTest() {
		@Nullable final User user = userRepository.findUserByLogin("user");
		@Nullable final User admin = userRepository.findUserByLogin("admin");
		assertNotNull(user);
		assertNotNull(admin);
		task.setUser(user);
		taskRepository.save(task);
		assertNotNull(taskRepository.findByUserIdAndId(user.getId(), task.getId()));
		taskRepository.deleteByUserIdAndId(admin.getId(), task.getId());
		assertNotNull(taskRepository.findByUserIdAndId(user.getId(), task.getId()));
		taskRepository.deleteByUserIdAndId(user.getId(), task.getId());
		assertNull(taskRepository.findByUserIdAndId(user.getId(), task.getId()));
		task = null;
	}

}