package ru.ahmetahunov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.dto.UserDTO;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

	@Column(unique = true, nullable = false)
	private String login;

	@Column(nullable = false)
	private String password;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<Role> roles = new ArrayList<>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Project> projects = new ArrayList<>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Task> tasks = new ArrayList<>();

	public UserDTO transformToDTO() {
		@NotNull final UserDTO userDTO = new UserDTO();
		userDTO.setId(this.id);
		userDTO.setLogin(this.login);
		userDTO.setPassword(this.password);
		for (Role role : roles)
			userDTO.getRoles().add(role.getType());
		return userDTO;
	}

}
