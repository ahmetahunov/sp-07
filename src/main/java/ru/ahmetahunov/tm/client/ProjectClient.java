package ru.ahmetahunov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.dto.ProjectDTO;
import java.util.List;

@FeignClient
public interface ProjectClient {

	static ProjectClient client(final String baseUrl) {
		final FormHttpMessageConverter converter = new FormHttpMessageConverter();
		final HttpMessageConverters converters = new HttpMessageConverters(converter);
		final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
		return Feign.builder()
				.contract(new SpringMvcContract())
				.encoder(new SpringEncoder(objectFactory))
				.decoder(new SpringDecoder(objectFactory))
				.target(ProjectClient.class, baseUrl);
	}

	@GetMapping(value = "/projects", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<ProjectDTO> getProjects(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/projects", consumes = MediaType.APPLICATION_XML_VALUE)
	public List<ProjectDTO> getProjectsXML(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/projects/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ProjectDTO getProject(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") String id
	);

	@GetMapping(value = "/projects/{id}", consumes = MediaType.APPLICATION_XML_VALUE)
	public ProjectDTO getProjectXML(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") String id
	);

	@PostMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public ProjectDTO addProject(
			@RequestHeader("Authorization") String header,
			@RequestBody ProjectDTO projectDTO
	);

	@PostMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public ProjectDTO addProjectXML(
			@RequestHeader("Authorization") String header,
			@RequestBody ProjectDTO projectDTO
	);

	@PutMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public ProjectDTO updateProject(
			@RequestHeader("Authorization") String header,
			@RequestBody ProjectDTO projectDTO
	);

	@PutMapping(
			value = "/projects",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public ProjectDTO updateProjectXML(
			@RequestHeader("Authorization") String header,
			@RequestBody ProjectDTO projectDTO
	);

	@DeleteMapping("/projects/{id}")
	public void removeProject(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") String id
	);

}
