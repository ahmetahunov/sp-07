package ru.ahmetahunov.tm.client;

import feign.Feign;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.dto.TaskDTO;
import java.util.List;

@FeignClient
public interface TaskClient {

	static TaskClient client(final String baseUrl) {
		final FormHttpMessageConverter converter = new FormHttpMessageConverter();
		final HttpMessageConverters converters = new HttpMessageConverters(converter);
		final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
		return Feign.builder()
				.contract(new SpringMvcContract())
				.encoder(new SpringEncoder(objectFactory))
				.decoder(new SpringDecoder(objectFactory))
				.target(TaskClient.class, baseUrl);
	}

	@GetMapping(value = "/tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<TaskDTO> getTasks(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/tasks", consumes = MediaType.APPLICATION_XML_VALUE)
	public List<TaskDTO> getTasksXML(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/tasks/project/{projectId}")
	public List<TaskDTO> getTasksByProjectId(
			@RequestHeader("Authorization") String header,
			@PathVariable("projectId") @NotNull final String projectId
	);

	@GetMapping(value = "/tasks/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public TaskDTO getTask(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

	@GetMapping(value = "/tasks/{id}",consumes = MediaType.APPLICATION_XML_VALUE)
	public TaskDTO getTaskXML(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

	@PostMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public TaskDTO addTask(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@PostMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public TaskDTO addTaskXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@PutMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public TaskDTO updateTask(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@PutMapping(
			value = "/tasks",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public TaskDTO updateTaskXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final TaskDTO taskDTO
	);

	@DeleteMapping("/tasks/{id}")
	public void removeTask(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

}
