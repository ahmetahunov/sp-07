package ru.ahmetahunov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.dto.UserDTO;

import java.util.List;

@FeignClient
public interface UserClient {

	static UserClient client(final String baseUrl) {
		final FormHttpMessageConverter converter = new FormHttpMessageConverter();
		final HttpMessageConverters converters = new HttpMessageConverters(converter);
		final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
		return Feign.builder()
				.contract(new SpringMvcContract())
				.encoder(new SpringEncoder(objectFactory))
				.decoder(new SpringDecoder(objectFactory))
				.target(UserClient.class, baseUrl);
	}

	@GetMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<UserDTO> getUsers(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/users", consumes = MediaType.APPLICATION_XML_VALUE)
	public List<UserDTO> getUsersXML(@RequestHeader("Authorization") String header);

	@GetMapping(value = "/users/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public UserDTO getUser(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

	@GetMapping(value = "/users/{id}",consumes = MediaType.APPLICATION_XML_VALUE)
	public UserDTO getUserXML(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

	@PostMapping(
			value = "/users",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public UserDTO createUser(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PostMapping(
			value = "/users",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public UserDTO createUserXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PostMapping(
			value = "/users/admin",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public UserDTO createUserAdmin(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PostMapping(
			value = "/users/admin",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public UserDTO createUserAdminXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PutMapping(
			value = "/users",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public UserDTO editUser(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PutMapping(
			value = "/users",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public UserDTO editUserXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PutMapping(
			value = "/users/admin",
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE
	)
	public UserDTO editUserAdmin(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@PutMapping(
			value = "/users/admin",
			produces = MediaType.APPLICATION_XML_VALUE,
			consumes = MediaType.APPLICATION_XML_VALUE
	)
	public UserDTO editUserAdminXML(
			@RequestHeader("Authorization") String header,
			@RequestBody @Nullable final UserDTO userDTO
	);

	@DeleteMapping("/users")
	public void removeUser(
			@RequestHeader("Authorization") String header
	);

	@DeleteMapping("/users/{id}")
	public void removeUserAdmin(
			@RequestHeader("Authorization") String header,
			@PathVariable("id") @NotNull final String id
	);

}
