package ru.ahmetahunov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ahmetahunov.tm.enumerated.RoleType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

	@NotNull
	private String login = "";

	@NotNull
	private List<RoleType> roles = new ArrayList<>();

	@NotNull
	private String passwordNew = "";

	@NotNull
	private String password = "";

	@NotNull
	private String passwordCheck = "";

}
