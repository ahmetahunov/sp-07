package ru.ahmetahunov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.Project;
import java.util.List;

public interface IProjectService extends IAbstractService<Project> {

	@Nullable
	public Project findOne(String userId, String id);

	@NotNull
	public List<Project> findAll(String userId);

	public void remove(String userId, String id);

}
