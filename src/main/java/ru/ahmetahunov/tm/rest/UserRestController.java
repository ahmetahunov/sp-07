package ru.ahmetahunov.tm.rest;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.entity.Role;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.RoleType;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserRestController {

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@Setter
	@NotNull
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@GetMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public List<UserDTO> getUsers() {
		return userService.findAll().stream().map(User::transformToDTO).collect(Collectors.toList());
	}

	@GetMapping(
			value = "/users/{id}",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public UserDTO getUser(@PathVariable("id") @NotNull final String id) {
		@Nullable final User user = userService.findOne(id);
		return (user == null) ? null : user.transformToDTO();
	}

	@PostMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO createUser(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@NotNull final User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		@NotNull final Role role = new Role();
		role.setUser(user);
		user.getRoles().add(role);
		return userService.persist(user).transformToDTO();
	}

	@PostMapping(
			value = "/users/admin",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public UserDTO createUserAdmin(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@NotNull final User user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		return userService.persist(user).transformToDTO();
	}

	@PutMapping(
			value = "/users",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	public UserDTO editUser(
			@NotNull final Principal principal,
			@RequestBody@Nullable final UserDTO userDTO
	) {
		if (userDTO == null) return null;
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return null;
		user.setLogin(userDTO.getLogin());
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPasswordNew()));
		return userService.merge(user).transformToDTO();
	}

	@PutMapping(
			value = "/users/admin",
			produces = {
					MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE
			})
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public UserDTO editUserAdmin(@RequestBody @Nullable final UserDTO userDTO) {
		if (userDTO == null) return null;
		@Nullable final User user = userService.findOne(userDTO.getId());
		if (user == null) return null;
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPasswordNew()));
		user.getRoles().clear();
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		return userService.merge(user).transformToDTO();
	}

	@DeleteMapping("/users")
	@PreAuthorize("isAuthenticated()")
	public void removeUser(@NotNull final Principal principal) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return;
		userService.remove(user.getId());
	}

	@DeleteMapping("/users/{id}")
	@PreAuthorize("hasRole('ADMINISTRATOR')")
	public void removeUser(@PathVariable("id") @NotNull final String id) {
		userService.remove(id);
	}

}
