package ru.ahmetahunov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.ahmetahunov.tm.entity.Task;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    public List<Task> findAllTasksByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    public List<Task> findAllByUserId(@NotNull final String userId);

    @Nullable
    public Task findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    public void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
