package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.dto.UserDTO;
import ru.ahmetahunov.tm.entity.Role;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.RoleType;
import javax.annotation.PostConstruct;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@Setter
	@NotNull
	@Autowired
	private PasswordEncoder passwordEncoder;

	@NotNull
	@Value("${admin.login}")
	private String adminLogin;

	@NotNull
	@Value("${admin.password}")
	private String adminPassword;

	@NotNull
	@Value("${user.login}")
	private String userLogin;

	@NotNull
	@Value("${user.password}")
	private String userPassword;

	@PostConstruct
	private void initUsers() {
		@Nullable final User check = userService.findByLogin(adminLogin);
		if (check == null) {
			@NotNull final User admin = new User();
			admin.setLogin(adminLogin);
			admin.setPassword(passwordEncoder.encode(adminPassword));
			@NotNull final Role roleUser = new Role();
			roleUser.setUser(admin);
			admin.getRoles().add(roleUser);
			@NotNull final Role roleAdmin = new Role();
			roleAdmin.setUser(admin);
			roleAdmin.setType(RoleType.ADMINISTRATOR);
			admin.getRoles().add(roleAdmin);
			userService.persist(admin);
		}
		@Nullable final User checkUser = userService.findByLogin(userLogin);
		if (checkUser != null) return;
		@NotNull final User user = new User();
		user.setLogin(userLogin);
		user.setPassword(passwordEncoder.encode(userPassword));
		@NotNull final Role role = new Role();
		role.setUser(user);
		user.getRoles().add(role);
		userService.persist(user);
	}

	@GetMapping("/registration")
	public String registration() {
		return "user/registration";
	}

	@PostMapping("/registration")
	public String registration(@NotNull final UserDTO userDTO) {
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		@Nullable User user = userService.findByLogin(userDTO.getLogin());
		if (user != null) return "/user_exists";
		user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		@NotNull final List<Role> roles = new ArrayList<>();
		@NotNull final Role role = new Role();
		role.setUser(user);
		role.setType(RoleType.USER);
		roles.add(role);
		user.setRoles(roles);
		userService.persist(user);
		return "redirect:/login";
	}

	@GetMapping("/admin/user_list")
	public String userList(@NotNull final Model model) {
		model.addAttribute("users", userService.findAll());
		return "user/admin_user_list";
	}

	@GetMapping("/admin/user_edit/{id}")
	public String userEditAdmin(
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("user", userService.findOne(id));
		return "user/admin_user_edit";
	}

	@PostMapping("/admin/user_edit")
	public String userEditAdmin(@NotNull final UserDTO userDTO) {
		@Nullable final User user = userService.findOne(userDTO.getId());
		if (user == null) return "/error_page";
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(passwordEncoder.encode(userDTO.getPasswordNew()));
		user.getRoles().clear();
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		userService.merge(user);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/user_edit/{id}")
	public String userEdit(
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		model.addAttribute("user", userService.findOne(id));
		return "user/user_edit";
	}

	@PostMapping("/user/user_edit")
	public String userEdit(
			@NotNull final UserDTO userDTO,
			@NotNull final Principal principal
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "/error_page";
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		if (!passwordEncoder.matches(userDTO.getPassword(), user.getPassword())) return "/error_page";
		user.setLogin(userDTO.getLogin());
		if (!userDTO.getPasswordNew().isEmpty())
			user.setPassword(passwordEncoder.encode(userDTO.getPasswordNew()));
		userService.merge(user);
		return "redirect:/";
	}

	@GetMapping("/admin/registration")
	public String registrationAdmin() {
		return "user/admin_registration";
	}

	@PostMapping("/admin/registration")
	public String registrationAdmin(@NotNull final UserDTO userDTO) {
		if (!userDTO.getPassword().equals(userDTO.getPasswordCheck())) return "/error_page";
		@Nullable User user = userService.findByLogin(userDTO.getLogin());
		if (user != null) return "/user_exists";
		user = new User();
		user.setLogin(userDTO.getLogin());
		user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		for (@NotNull final RoleType roleType : userDTO.getRoles()) {
			@NotNull final Role role = new Role();
			role.setUser(user);
			role.setType(roleType);
			user.getRoles().add(role);
		}
		userService.persist(user);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/delete/{id}")
	public String removeUser(
			@PathVariable("id") @NotNull final String id,
			@NotNull final Principal principal
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null || !user.getId().equals(id)) return "/error_page";
		userService.remove(user.getId());
		return "redirect:/logout";
	}

	@GetMapping("/admin/delete/{id}")
	public String removeUser(@PathVariable("id") @NotNull final String id) {
		userService.remove(id);
		return "redirect:/admin/user_list";
	}

	@GetMapping("/user/user_info")
	public String getInfo(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		model.addAttribute("user", userService.findByLogin(principal.getName()));
		return "user/user_info";
	}

}
