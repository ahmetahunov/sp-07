package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.ITaskService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import java.security.Principal;
import java.util.List;

@Controller
public class TaskController {

	@Setter
	@NotNull
	@Autowired
	private ITaskService taskService;

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public String errorHandle() {
		return "exception";
	}

	@GetMapping("/task_list")
	public String taskList(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		model.addAttribute("tasks", taskService.findAll(user.getId()));
		return "task/task_list";
	}

	@GetMapping("/task_list/{projectId}")
	public String taskListByProjectId(
			@NotNull final Principal principal,
			@PathVariable("projectId") @NotNull final String id,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		@NotNull final List<Task> tasks = taskService.findAll(user.getId(), id);
		model.addAttribute("tasks", tasks);
		model.addAttribute("project", projectService.findOne(id));
		return "task/task_list_by_project";
	}

	@GetMapping("/task_info/{id}")
	public String taskInfo(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_info";
		model.addAttribute("task", taskService.findOne(user.getId(), id));
		return "task/task_info";
	}

	@GetMapping("/task_create")
	public String taskCreate(
			@NotNull final Principal principal,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_info";
		model.addAttribute("projects", projectService.findAll(user.getId()));
		return "task/task_create";
	}

	@PostMapping("/task_create")
	public String taskCreate(
			@NotNull final Principal principal,
			@NotNull final Task task,
			@ModelAttribute("projectId") @NotNull final String projectId
	) throws InterruptedOperationException {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		if (task.getName().trim().isEmpty()) throw new InterruptedOperationException();
		@NotNull final Project project = new Project();
		project.setId(projectId);
		project.setUser(user);
		task.setProject(project);
		task.setUser(user);
		taskService.persist(task);
		return "redirect:/task_list";
	}

	@GetMapping("/task_update/{id}")
	public String taskUpdate(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) throws InterruptedOperationException {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		@Nullable final Task task = taskService.findOne(user.getId(), id);
		if (task == null) throw new InterruptedOperationException();
		model.addAttribute("taskEdit", task);
		model.addAttribute("projectEdit", task.getProject());
		model.addAttribute("projects", projectService.findAll());
		return "task/task_update";
	}

	@PostMapping("/task_update")
	public String taskUpdate(
			@NotNull final Principal principal,
			@NotNull final Task task,
			@ModelAttribute("projectId") @NotNull final String projectId,
			@NotNull final Model model
	) throws InterruptedOperationException {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		@Nullable final Task found = taskService.findOne(user.getId(), task.getId());
		if (found == null) throw new InterruptedOperationException();
		if (!task.getName().trim().isEmpty())found.setName(task.getName());
		@NotNull final Project project = new Project();
		project.setId(projectId);
		project.setUser(user);
		found.setProject(project);
		found.setDescription(task.getDescription());
		found.setStartDate(task.getStartDate());
		found.setFinishDate(task.getFinishDate());
		found.setStatus(task.getStatus());
		model.addAttribute("task", taskService.merge(found));
		return "task/task_info";
	}

	@GetMapping("/task_delete/{id}")
	public String taskDelete(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "task/task_list";
		taskService.remove(user.getId(), id);
		return "redirect:/task_list";
	}
	
}
