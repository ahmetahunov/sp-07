package ru.ahmetahunov.tm.controller;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.entity.Project;
import java.security.Principal;

@Controller
public class ProjectController {

	@Setter
	@NotNull
	@Autowired
	private IProjectService projectService;

	@Setter
	@NotNull
	@Autowired
	private IUserService userService;

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = Exception.class)
	public String errorHandle() {
		return "exception";
	}

	@GetMapping("/project_list")
	public String projectList(@NotNull final Principal principal, @NotNull final Model model) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_list";
		model.addAttribute("projects", projectService.findAll(user.getId()));
		return "project/project_list";
	}

	@GetMapping("/project_info/{id}")
	public String projectInfo(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_info";
		model.addAttribute("project", projectService.findOne(user.getId(), id));
		return "project/project_info";
	}

	@GetMapping("/project_create")
	public String projectCreate() {
		return "project/project_create";
	}

	@PostMapping("/project_create")
	public String projectCreate(
			@NotNull final Principal principal,
			@NotNull final Project project
	) throws InterruptedOperationException {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_list";
		if (project.getName().trim().isEmpty()) throw new InterruptedOperationException();
		project.setUser(user);
		projectService.persist(project);
		return "redirect:/project_list";
	}

	@GetMapping("/project_update/{id}")
	public String projectUpdate(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id,
			@NotNull final Model model
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_list";
		model.addAttribute("projectEdit", projectService.findOne(user.getId(), id));
		return "project/project_update";
	}

	@PostMapping("/project_update")
	public String projectUpdate(
			@NotNull final Principal principal,
			@NotNull final Project project,
			@NotNull final Model model
	) throws InterruptedOperationException {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_list";
		@Nullable final Project found = projectService.findOne(user.getId(), project.getId());
		if (found == null) throw new InterruptedOperationException();
		if (!project.getName().isEmpty())found.setName(project.getName());
		found.setDescription(project.getDescription());
		found.setStartDate(project.getStartDate());
		found.setFinishDate(project.getFinishDate());
		found.setStatus(project.getStatus());
		model.addAttribute("project", projectService.merge(found));
		return "project/project_info";
	}

	@GetMapping("/project_delete/{id}")
	public String projectDelete(
			@NotNull final Principal principal,
			@PathVariable("id") @NotNull final String id
	) {
		@Nullable final User user = userService.findByLogin(principal.getName());
		if (user == null) return "project/project_list";
		projectService.remove(user.getId(), id);
		return "redirect:/project_list";
	}

}
