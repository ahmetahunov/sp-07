<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task create</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
    <jsp:include page="../header.jsp"/>
    <jsp:useBean id="now" class="java.util.Date"/>
    <fmt:formatDate value="${now}" var="defaultDate" pattern="yyyy-MM-dd"/>
    <div class="center">
    <div class="table">
        <c:if test="${projects.size() == 0}">
            <h1>Please create Project first!</h1>
        </c:if>
        <form name="task" action="${pageContext.request.contextPath}/task_create" method="post">
            <c:if test="${projects.size() != 0}">
                <table class="info">
                    <caption>TASK CREATE</caption>
                    <tr>
                        <td>Project:</td>
                        <td>
                            <select name="projectId">
                                <c:forEach items="${projects}" var="project">
                                    <option value="${project.id}">${project.name}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td><input type="text" id="name" name="name" required></td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td><input type="text" id="description" name="description"></td>
                    </tr>
                    <tr>
                        <td>Start date:</td>
                        <td><input type="date" id="startDate" name="startDate" value="${defaultDate}" required></td>
                    </tr>
                    <tr>
                        <td>Finish date:</td>
                        <td><input type="date" id="finishDate" name="finishDate" value="${defaultDate}" required></td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td>
                            <select name="status" required>
                                <option value="PLANNED">PLANNED</option>
                                <option value="IN_PROGRESS">IN-PROGRESS</option>
                                <option value="DONE">DONE</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="SAVE" class="button green">
            </c:if>
        </form>
    </div>
    </div>
    <jsp:include page="../footer.jsp"/>
</body>
</html>
