<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User edit</title>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/nav.css"/>" type="text/css"/>
    <link rel="stylesheet" href="<c:url value=" ${pageContext.request.contextPath}/css/table.css"/>" type="text/css"/>
</head>
<body>
<jsp:include page="../header.jsp"/>
<div class="center">
    <div class="table">
        <form name="userDTO" action="${pageContext.request.contextPath}/user/user_edit" method="post">
            <table class="info">
                <caption>USER EDIT</caption>
                <tr>
                    <td>ID:</td>
                    <td><input name="id" value="${user.id}" readonly></td>
                </tr>
                <tr>
                    <td>Login:</td>
                    <td><input type="text" id="login" name="login" value="${user.login}" required></td>
                </tr>
                <tr>
                    <td>New password:</td>
                    <td><input type="password" id="passwordNew" name="passwordNew"></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td><input type="password" id="password" name="password" required></td>
                </tr>
                <tr>
                    <td>Password check:</td>
                    <td><input type="password" id="passwordCheck" name="passwordCheck" required></td>
                </tr>
            </table>
            <input type="submit" value="UPDATE" class="button green">
        </form>
    </div>
</div>
<jsp:include page="../footer.jsp"/>
</body>
</html>

