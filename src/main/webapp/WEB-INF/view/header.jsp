<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header>
    <nav>
        <a href="${pageContext.request.contextPath}/"><div class="title">TASK MANAGER</div></a>
        <a href="${pageContext.request.contextPath}/" class="link">HOME</a>
        <sec:authorize access="isAuthenticated()">
            <div class="brake">|</div>
            <a href="${pageContext.request.contextPath}/project_list" class="link">PROJECT</a>
            <div class="brake">|</div>
            <a href="${pageContext.request.contextPath}/task_list" class="link">TASK</a>
        </sec:authorize>
        <sec:authorize access="hasRole('ADMINISTRATOR')">
            <div class="brake">|</div>
            <a href="${pageContext.request.contextPath}/admin/user_list" class="link">USERS</a>
        </sec:authorize>
    </nav>
    <div class="user_info">
        <sec:authorize access="!isAuthenticated()">
            <a class="btn" href="<c:url value="${pageContext.request.contextPath}/login" />" role="button">
                <button>LOG-IN</button>
            </a>
            <a class="btn" href="<c:url value=" ${pageContext.request.contextPath}/registration" />" role="button">
                <button>REGISTRATION</button>
            </a>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <a href="${pageContext.request.contextPath}/user/user_info" class="name">
                <p>User: <sec:authentication property="principal.username" /></p>
            </a>
            <a class="btn" href="<c:url value="${pageContext.request.contextPath}/logout" />">
                <button>LOG-OUT</button>
            </a>
        </sec:authorize>
    </div>
</header>
