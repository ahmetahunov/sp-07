https://gitlab.com/ahmetahunov/sp-07
# TASK MANAGER

## SOFTWARE:
+ Git
+ JRE
+ Java 8
+ Maven
+ PostgreSQL
+ Tomcat

## Developer

  Rustamzhan Akhmetakhunov\
  email: ahmetahunov@yandex.ru

## build app

```bash
git clone http://gitlab.volnenko.school/ahmetahunov/sp-07.git
cd sp-07
mvn clean install
```

## run app
```bash
mvn spring-boot:run
```